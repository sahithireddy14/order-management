package com.dbs.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.dbs.order.client.OrderItemServiceClient;
import com.dbs.order.common.dto.Item;
import com.dbs.order.common.dto.Orders;
import com.dbs.order.exception.ItemNotFoundException;
import com.dbs.order.repository.OrderRepository;

@Service
public class OrderService implements IOrderService{

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderItemServiceClient orderItemServiceClient;

	@Override
	public void createOrders(List<Orders> orders) {
		orders.parallelStream().forEach(order -> {
		    List<Item> items = orderItemServiceClient.getOrderItemsByProductCodes(order.getProductCodes());
		    order.setItem(items);
		    orderRepository.save(order);
		});		
	}

	@Override
	public List<Orders> retrieveOrders() {
		return orderRepository.findAll();
	}

}
